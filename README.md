# rhino-linux-gnome-guide

Guide to install Gnome / Ubuntu desktop on Rhino Linux

1.    After installing Rhino Linux and rebooting, make sure you are connected to Internet

2.    Do the Setup Wizard thing. Do Reboot when asked so.

3.    Open the application named "Your System" and perform "System Upgrade"

4.    Reboot (cuz your Linux kernel may have been updated)

5.    Open any terminal (eg. xfce-terminal) and input the following command 
  `sudo nala install ubuntu-desktop-minimal
`
You can also install the bloated variant named `ubuntu-desktop`

5. During installation, you will be asked which Login Manager to use, I would recommend you to choose `gdm` however `lightdm` should also work fine

6. Reboot

7. Login to Ubuntu desktop, default session is Wayland

8. Remove the "Software and Updates" and "Software Updater" applications using 

    `sudo nala remove software-properties-gtk update-manager`

9. Install gnome tweaks

    `sudo nala install gnome-tweaks`

10. Launch Tweaks, go to Startup Applications, Remove Plank Dock, Plank Theme and Ulauncher